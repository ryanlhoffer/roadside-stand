import 'dotenv/config'
import cors from 'cors'

import express from 'express'
import swaggerUi from 'swagger-ui-express'
import YAML from 'yamljs'

/* import routers */
import { router as standRouter } from './routers/stand-router.js'
import { router as searchRouter } from './routers/search-router.js'

/* create an express app and use JSON */
const app = new express()
app.use(express.json())
app.use(cors({
    origin: '*'
}));


/* bring in some routers */
app.use('/stand', standRouter)
app.use('/stands', searchRouter)



/* set up swagger in the root */
const swaggerDocument = YAML.load('api.yaml')
app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument))

/* start the server */
app.listen(8080)