import { Entity, Schema } from 'redis-om'
import client from './client.js'

class Stand extends Entity {}

/* create a Schema for Stand */
const standSchema = new Schema(Stand, {
    name: { type: 'string' },
    address: { type: 'string' },
    goods: { type: 'string[]' },
    location: { type: 'point' },
    cash_only: {type: 'boolean'}
  })

  /* use the client to create a Repository just for Stands */
export const standRepository = client.fetchRepository(standSchema)

/* create the index for Stand */
await standRepository.createIndex()