import { Router } from 'express'
import { standRepository } from '../om/stand.js'

export const router = Router()

router.get('/all', async (req, res) => {
    const stands = await standRepository.search().return.all()
    res.send(stands)
  })