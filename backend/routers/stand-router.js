import { Router } from 'express'
import { standRepository } from '../om/stand.js'

export const router = Router();

router.put('/', async (req, res) => {
    const stand = await standRepository.createAndSave(req.body)
    res.send(stand)
})

router.get('/:id', async (req, res) => {
    const stand = await standRepository.fetch(req.params.id)
    res.send(stand)
})

router.post('/:id', async (req, res) => {
    const stand = await standRepository.fetch(req.params.id)

    stand.name = req.body.name ?? null
    stand.address = req.body.address ?? null
    stand.goods = req.body.goods ?? null
    stand.location = req.body.location ?? null
    stand.cash_only = req.body.cash_only ?? null

    await standRepository.save(stand)

    res.send(stand)
})

router.delete('/:id', async (req, res) => {
    await standRepository.remove(req.params.id)
    res.send({ entityId: req.params.id })
})